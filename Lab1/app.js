const addDeleteStudentActionToButton = (button) => {
    button.addEventListener("click", () => {
        const row = button.closest("tr");
        const fullName = row.querySelector('td.cell-name');

        const deleteModal = document.querySelector(".delete-modal");
        const warningModalContent = document.querySelector(".delete-modal .modal-body");
        const approveButton = document.querySelector(".delete-modal .approve");
        const cancelButton = document.querySelector(".delete-modal .cancel");
        const closeButton = document.querySelector(".delete-modal .close-modal");

        warningModalContent.textContent += fullName.textContent;
        warningModalContent.textContent += " ?";
        deleteModal.classList.add("show-modal");

        approveButton.addEventListener("click", () => {
            row.remove();
            deleteModal.classList.remove("show-modal");
            warningModalContent.textContent = warningModalContent.textContent.replace(fullName.textContent, '');
        });

        cancelButton.addEventListener("click", () => {
            deleteModal.classList.remove("show-modal");
            warningModalContent.textContent = warningModalContent.textContent.replace(fullName.textContent, '');

        });
        closeButton.addEventListener("click", () => {
            deleteModal.classList.remove("show-modal");
            warningModalContent.textContent = warningModalContent.textContent.replace(fullName.textContent, '');
        });
    });
}

const markDeleteButtons = () => {
    const deleteButtons = document.querySelectorAll(".delete-btn");
    deleteButtons.forEach((button) => {
        addDeleteStudentActionToButton(button);
    });
}

const addAnimationToNotification = () => {
    const notification = document.querySelector(".notification-image");
    notification.addEventListener('dblclick', () => {
        notification.classList.remove('bell-animation');
        notification.offsetWidth;
        notification.classList.add('bell-animation');
    })
}

const addStudentAction = () => {
    const addStudentButton = document.querySelector(".add-student-btn");
    const addStudentModal = document.querySelector(".edit-modal");
    const closeModalBtn = addStudentModal.querySelector(".close-modal");
    const createStudentBtn = addStudentModal.querySelector(".create");

    addStudentButton.addEventListener("click", () => (addStudentModal.classList.add('show-modal')));
    closeModalBtn.addEventListener("click", () => (addStudentModal.classList.remove('show-modal')));
    createStudentBtn.addEventListener("click", () => {
        const generateNewStudent = (status, group, name, gender, birthday) => {
            return `<td class="cell-checkbox">
				<label class="checkbox-container">
					<input type="checkbox" ${status === "active" && "checked"} />
				</label>
			</td>
			<td>${group}</td>
			<td class="cell-name">${name}</td>
			<td>${gender}</td>
			<td>${birthday}</td>
			<td><span class="cell-status ${status === "active" ? "active-status" : "non-status"} student-status">
            </span></td>
				<td class="cell-options">
				<button><img src="./images/edit.svg" alt="edit" /></button
				><button class="delete-btn">
					<img src="./images/delete.svg" alt="delete" />
				</button>
			</td>`;
        };

        const table = document.querySelector(".student-table");
        const row = table.insertRow();
        row.innerHTML = generateNewStudent('active', 'PZ-25', 'Wtf Wtf', 'M', '06.06.2006');
        addStudentModal.classList.remove('show-modal');
        const button = row.querySelector('.delete-btn');
        addDeleteStudentActionToButton(button);
    });
};

const addActionButtons = () => {
    addStudentAction();
    markDeleteButtons();
    addAnimationToNotification();
};

addActionButtons();