import React, { useEffect, useState } from "react";
import { MdOutlineModeEditOutline } from "react-icons/md";
import { IoMdClose } from "react-icons/io";
import className from "classnames";
import moment from "moment";
import { Button, Form } from "react-bootstrap";
import { DeleteModal } from "../DeleteModal/DeleteModal";
import { AddEditModal } from "../AddEditModal/AddEditModal";
import "./StudentsTableCell.scss";

export const StudentsTableCell = ({
  id,
  status,
  group,
  firstName,
  lastName,
  gender,
  birthday,
  deleteStudent,
  editStudent,
  groups
}) => {
  const [userChecked, setUserChecked] = useState(status === "active");
  const [removeModal, setRemoveModal] = useState(false);
  const [editModal, setEditModal] = useState(false);

  const handleRemoval = () => {
    setRemoveModal(false);
    deleteStudent(id);
  };

  const handleUpdate = (student) => {
    setEditModal(false);
    editStudent({
      ...student,
      id,
      birthday: moment(student.birthday).format("DD.MM.YYYY"),
    });
  };

  useEffect(() => {
    setUserChecked(status === "active");
  }, [status]);

  return (
    <>
      <tr>
        <td>
          <Form className="student-table-checkbox">
            <Form.Check
              type="checkbox"
              label=""
              id="checkbox"
              onChange={() => setUserChecked(!userChecked)}
              checked={userChecked}
            />
          </Form>
        </td>
        <td>{group}</td>
        <td>{firstName + " " + lastName}</td>
        <td>{gender?.[0]?.toUpperCase()}</td>
        <td>{moment(birthday, "YYYY-MM-DD").format("DD.MM.YYYY")}</td>
        <td>
          <span
            className={className("status", {
              active: userChecked,
              inactive: !userChecked,
            })}
          ></span>
        </td>
        <td>
          <Button onClick={() => setEditModal(true)} variant="light">
            <MdOutlineModeEditOutline size={20} />
          </Button>{" "}
          <Button onClick={() => setRemoveModal(true)} variant="light">
            <IoMdClose size={20} />
          </Button>
        </td>
      </tr>
      <DeleteModal
        showModal={removeModal}
        onCancelModal={() => setRemoveModal(false)}
        onApproveModal={handleRemoval}
        student={firstName + " " + lastName}
      />
      <AddEditModal
        showModal={editModal}
        onCancelModal={() => setEditModal(false)}
        mode="editing"
        onApproveModal={handleUpdate}
        groups={groups}
        defaultValues={{
          firstName,
          lastName,
          group,
          gender,
          birthday
        }}
      />
    </>
  );
};
