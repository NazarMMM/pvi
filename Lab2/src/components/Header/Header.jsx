import React, {useEffect, useState} from "react";
import classNames from "classnames";
import { BsPersonCircle, BsBell } from "react-icons/bs";

import "./Header.scss";
import { useNavigate } from "react-router-dom";
import { getLastThreeOrLessMessages, getUserById } from "../../service/studentService";


const Header = ({ socket, id, createdRoom }) => {
  const [receivedMessage, setReceivedMessage] = useState(false);
  const [currentUser, setCurrentUser] = useState({});
  const [userLastMessages, setUserLastMessages] = useState([]);
  const navigate = useNavigate();

  const getAndShowMostRecentMessages = async () => {
    const messages = await getLastThreeOrLessMessages(id, 3);
    setUserLastMessages(messages);
  };

  const getUserAndMessages = async () => {
    const user = await getUserById(id);
    setCurrentUser(user);
    await getAndShowMostRecentMessages();
  };
  
  const prepareData = async () => {
    await getUserAndMessages();
    socket.on("message", (message) => {
      console.log(message.from);
      console.log(id);
      if (message.from !== id) {
        setReceivedMessage(true);
      }
      getAndShowMostRecentMessages();
    });
  };

  useEffect(() => {
    prepareData();
  }, [createdRoom]);

  return (
    <header className="header">
      <div className="header-logo">CMS</div>
      <div className="header-right-side">
        <div className="notification">
          <div className="notification-image">
            <div
              className={classNames({ "notification-animation": receivedMessage })}
              onAnimationEnd={() => setReceivedMessage(false)}
              onClick={() => navigate("/chat")}>
              <BsBell fill="#fff" size={30} />
            </div>
            <div
                className={classNames("indicator", { "show-indicator": receivedMessage })}
            ></div>
          </div>
          <div className="show-dropdown">
            {userLastMessages?.map((message) => {
              return (
                  <div className="dropdown-element">
                    <div className="each-user">
                      <BsPersonCircle className="each-user-logo" size="30px" />
                      <div className="each-user-name"></div>
                    </div>
                    <div className="message">{message.message}</div>
                  </div>
              );
            })}
          </div>
        </div>
        <div className="user">
          <BsPersonCircle fill="#fff" size="37px" />
          <div id="logged-user" className="user-name">{currentUser?.username}</div>
          <div className="show-dropdown">
            <a href="#profile">Profile</a>
            <a href="#log-out">Log out</a>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
