import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";

import "./Navbar.scss";

const MainNavbar = () => {
  return (
    <Navbar className="main-navbar">
      <Nav className="navbar-items">
        <NavLink to="/dashboard">Dashboard</NavLink>
        <NavLink to="/students">Students</NavLink>
        <NavLink to="/tasks">Tasks</NavLink>
      </Nav>
    </Navbar>
  );
};

export default MainNavbar;
