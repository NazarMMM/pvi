import {Button} from "react-bootstrap";
import React, {useState} from "react";
import moment from "moment/moment";
import {AddEditTaskModal} from "../AddEditTaskModal/AddEditTaskModal";

export const Task = ({task, editTask}) => {
    const [editModal, setEditModal] = useState(false);

    const handleUpdateTask = (task) => {
        setEditModal(false);
        editTask({
            ...task,
            date: moment(task.date).format("DD.MM.YYYY")
        });
    };

    return (
        <>
            <Button onClick={() => setEditModal(true)} className="task-edit-btn" variant="light">
                <span className='align-left'>{task.name}</span>
                <span className='align-right'>{task.date}</span>
            </Button>
            <AddEditTaskModal
                showModal={editModal}
                onCancelModal={() => setEditModal(false)}
                mode="editing"
                onApproveModal={handleUpdateTask}
                defaultValues={task}
            />
        </>
    )
}
