import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

import "./AddEditModal.scss";
export const AddEditModal = ({
  showModal,
  onCancelModal,
  onApproveModal,
  mode,
  groups,
  defaultValues = {},
}) => {
  const [isStudentValidated, setIsStudentValidated] = useState(false);

  const addOrEditSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    setIsStudentValidated(true);
    if (form.checkValidity()) {
      onApproveModal({
        group: groups.find((group) => group.name === form.group.value)?.id,
        firstName: form.firstName.value,
        lastName: form.lastName.value,
        gender: form.gender.value,
        birthday: form.birthday.value,
      });
      setIsStudentValidated(false);
    }
  };

  const hideModal = () => {
    setIsStudentValidated(false);
    onCancelModal();
  };

  return (
    <>
      <Modal size="md" show={showModal} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>
            {mode === "editing" ? "Edit" : "Add"} student
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form noValidate={true} validated={isStudentValidated} onSubmit={addOrEditSubmit}>
            <Form.Group className="mb-3 form-group" controlId="group">
              <Form.Label>Group</Form.Label>
              <Form.Select defaultValue={defaultValues.group || ""} required>
                {groups?.map((group) => (
                  <option key={group.id} value={group.name}>
                    {group.name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="firstName">
              <Form.Label>First name</Form.Label>
              <Form.Control
                defaultValue={defaultValues.firstName || ""}
                required
                pattern="^[a-zA-Z]{1,}$"
                type="text"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="lastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                defaultValue={defaultValues.lastName || ""}
                pattern="^[a-zA-Z]{1,}$"
                required
                type="text"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="gender">
              <Form.Label>Gender</Form.Label>
              <Form.Select defaultValue={defaultValues.gender || ""} required>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="birthday">
              <Form.Label>Birthday</Form.Label>
              <Form.Control
                defaultValue={defaultValues.birthday || ""}
                required
                max="2012-12-31"
                min="2000-01-01"
                type="date"
              />
            </Form.Group>
            <Modal.Footer>
              <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}}
                      className="save-edit-btn" type="submit">
                {mode === "editing" ? "Save" : "Create"}
              </Button>
              <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} onClick={hideModal}>
                Cancel
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
