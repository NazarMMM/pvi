import React from "react";
import { Alert, Modal } from "react-bootstrap";
import "./ErrorMsgModal.scss";
const ErrorMsgModal = ({ show, hide, text }) => {
    return (
        <>
            <Modal centered className="error-modal" show={show} onHide={hide}>
                <Modal.Header className="modal-header" closeButton>
                </Modal.Header>
                <Modal.Body>
                    <Alert variant={"danger"}>
                        <Alert.Heading>Error</Alert.Heading>
                        <p className="new-line-error">{text}</p>
                    </Alert>
                </Modal.Body>
            </Modal>
        </>
    );
};

export default ErrorMsgModal;
