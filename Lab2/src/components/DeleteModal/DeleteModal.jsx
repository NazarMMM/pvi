import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

export const DeleteModal = ({
  showModal,
  onCancelModal,
  onApproveModal,
  student
}) => {
  return (
    <>
      <Modal show={showModal} onHide={onCancelModal}>
        <Modal.Header closeButton>
          <Modal.Title>Warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to delete user {student} ?
        </Modal.Body>
        <Modal.Footer>
          <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} onClick={onApproveModal}>
            Ok
          </Button>
          <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} onClick={onCancelModal}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
