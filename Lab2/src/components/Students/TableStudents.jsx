import React, { useEffect, useState } from "react";
import { BiPlus } from "react-icons/bi";
import { nanoid } from "nanoid";
import moment from "moment";
import { Button, Form, Table } from "react-bootstrap";
import { StudentsTableCell } from "../StudentsTableItem/StudentsTableCell";
import { AddEditModal } from "../AddEditModal/AddEditModal";
import {
  createStudentReq,
  getGroupsReq,
  getStudentsReq,
  updateStudentReq,
  deleteStudentByIdReq
} from "../../service/studentService";
import ErrorMsgModal from "../ErrorMsgModal/ErrorMsgModal";
import "./TableStudents.scss";

const TableStudents = () => {
  const [showModal, setShowModal] = useState(false);
  const [showErrorMsgModal, setShowErrorMsgModal] = useState(false);
  const [error, setError] = useState("");
  const [students, setStudents] = useState([]);
  const [groups, setGroups] = useState([]);

  const deleteStudent = async (id) => {
    await deleteStudentByIdReq(id);
    await fetchStudents();
  };

  const createStudent = async (student) => {
    const newStudent = {
      ...student,
      id: nanoid(),
      status: "inactive",
      birthday: moment(student.birthday).format("DD.MM.YYYY"),
    };
    setShowModal(false);
    try {
      await createStudentReq(newStudent);
      await fetchStudents();
    } catch (error) {
      setError(error?.response?.data?.error || "Server did not respond");
      setShowErrorMsgModal(true);
    }
  };

  const updateStudent = async (student) => {
    try {
      await updateStudentReq(student);
      await fetchStudents();
    } catch (error) {
      setError(error?.response?.data?.error || "Server did not respond");
      setShowErrorMsgModal(true);
    }
  };

  const fetchStudents = async () => {
    try {
      const students = await getStudentsReq();
      setStudents(students);
    } catch (error) {
      setError(error?.response?.data?.error || "Server did not respond");
      setShowErrorMsgModal(true);
    }
  };

  const getGroups = async () => {
    const newGroups = await getGroupsReq();
    setGroups(newGroups);
  };

  const fetchGroupsAndStudent = async () => {
    await fetchStudents();
    await getGroups();
  };

  useEffect(() => {
    try {
      fetchGroupsAndStudent();
    } catch (error) {
      setError(error?.response?.data?.error || "Server did not respond");
      setShowErrorMsgModal(true);
    }
  }, []);


  return (
    <div className="students-table">
      <div className="students-title-row">
        <h1 className="students-title">Students</h1>
        <Button onClick={() => setShowModal(true)} variant="light">
          <BiPlus size={20} />
        </Button>
      </div>
      <Table className="main-table" striped bordered hover>
        <thead>
          <tr>
            <th>
              <Form className="main-checkbox">
                <Form.Check
                  type="checkbox"
                  label=""
                  id="main-checkbox"
                />
              </Form>
            </th>
            <th>Group</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Birthday</th>
            <th>Status</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {students?.map((student) => {
            return (
              <StudentsTableCell
                deleteStudent={deleteStudent}
                key={student.id}
                editStudent={updateStudent}
                {...student}
                groups={groups}
              />
            );
          })}
        </tbody>
      </Table>
      <AddEditModal
        showModal={showModal}
        onCancelModal={() => setShowModal(false)}
        onApproveModal={createStudent}
        groups={groups}
      />
      <ErrorMsgModal
          show={showErrorMsgModal}
          hide={() => setShowErrorMsgModal(false)}
          text={error}
      />
    </div>
  );
};

export default TableStudents;
