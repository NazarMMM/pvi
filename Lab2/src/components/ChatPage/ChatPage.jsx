import React, { useEffect, useState } from "react";

import "./ChatPage.scss";
import ChatRooms from "../../components/UsersChatRooms/UsersChatRooms";
import ChatBody from "../../components/UserChat/UserChat";

import {getRoomsByUserId, getAllUsers, connectToAllRooms} from "../../service/studentService";

const ChatPage = ({ currentUser, socket, createdRoom, setCreatedRoom }) => {
  const [currentRoom, setCurrentRoom] = useState({});
  const [users, setUsers] = useState([]);
  const [messages, setMessages] = useState([]);
  const [messageSender, setMessageSender] = useState(currentUser);
  const [message, setMessage] = useState("");

  const [rooms, setRooms] = useState([]);

  useEffect(() => {
    socket.on("message", (newMessage) => {
      setMessages([...messages, newMessage]);
    });
    socket.on("messageHistory", (messages) => {
      setMessages(messages);
    });
  }, [messages]);

  const setUsersAndRooms = async () => {
    const users = await getAllUsers();
    const rooms = await getRoomsByUserId(currentUser);
    setUsers(users);
    setRooms(rooms);
  };

  useEffect(() => {
    setUsersAndRooms().then(data => data);
  }, [createdRoom]);

  const joinRoomEvent = (roomId) => {
    socket.emit("join", roomId, messageSender);
  };

  const createRoomEvent = (users) => {
    socket.emit("create-room", users);
  };

  const sendMessageEvent = (message) => {
    socket.emit("message", currentRoom._id, messageSender, message);
    setMessage("");
  };

  return (
    <div className="chat-page">
      <h1 className="title">Messages</h1>
      <main>
        <ChatRooms
          setCreatedRoom={setCreatedRoom}
          rooms={rooms}
          activeRoom={currentRoom}
          setActiveRoom={setCurrentRoom}
          joinRoom={joinRoomEvent}
          createRoom={createRoomEvent}
          users={users}
        />
        <ChatBody
          activeRoom={currentRoom}
          messages={messages}
          users={users}
          currentUser={currentUser}
          sendMessage={sendMessageEvent}
        />
      </main>
    </div>
  );
};

export default ChatPage;
