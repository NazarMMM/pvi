import React from "react";
import { IoPersonCircleSharp } from "react-icons/io5";
import UserMessage from "../UserMessage/UserMessage";
import { Button, Form, FormGroup } from "react-bootstrap";
import "./UserChat.scss";
const UserChat = ({
  messages,
  activeRoom,
  users,
  currentUser,
  sendMessage,
}) => {
  const joinMembersByUsername = () => {
      console.log(activeRoom);
    return activeRoom?.members?.map((id) => {
        return users.find((user) => user._id === id)?.username;
      })
        ?.join(", ");
  };

  const handleMessageSendingEvent = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    const value = form.message.value;
    if (value.trim().length) {
      sendMessage(value);
      form.message.value = "";
    }
  };
  return (
    <div className="user-chat-body">
      <h1 className="title remove-margin">Chat room {joinMembersByUsername()}</h1>
      <div className="all-members">
        <h2>Members</h2>
        <div className="all-members-list">
          {Array(activeRoom?.members?.length || 0)
            .fill(0)
            .map(() => <IoPersonCircleSharp size={25} />)}
        </div>
      </div>
      <h2 className="section-title">Messages</h2>

      <div className="messages">
        {messages?.map((el) => {
          const align = el.from === currentUser ? "right" : "left";
          return <UserMessage key={el._id} text={el.message} align={align} />;
        })}
      </div>
      <div className="send-message">
        <Form onSubmit={handleMessageSendingEvent}>
          <FormGroup className="input-message" controlId="message">
            <Form.Control />
            <Button type="submit">Send</Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  );
};
export default UserChat;
