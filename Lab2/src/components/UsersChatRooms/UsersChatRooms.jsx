import cn from "classnames";
import React, {useEffect, useState} from "react";
import { Button, Card } from "react-bootstrap";
import { IoPersonCircleSharp } from "react-icons/io5";
import { CreateRoomModal } from "../CreateRoomModal/CreateRoomModal";
import "./UsersChatRooms.scss";
const UsersChatRooms = ({
  activeRoom,
  joinRoom,
  createRoom,
  setActiveRoom: setCurrentRoom,
  setCreatedRoom,
  users,
  rooms
}) => {
  const [showModal, setShowModal] = useState(false);

  const joinMembersByUsername = (ids) => {
    return ids?.map((id) => {
        return users.find((user) => user._id === id)?.username;
      })
      ?.join(", ");
  };

  const selectRoom = (room) => {
    joinRoom(room._id);
    setCurrentRoom(room);
  };

  const createRoomForUsers = (users) => {
    createRoom(users);
    setShowModal(false);
    // setCurrentRoom(room);
  };

  const getAllUserNames = () => {
    return users.map(({password, ...user}) => user);
  };

  useEffect(() => {
    console.log(rooms);
    if (rooms?.length) {
      console.log(rooms[0]);
      selectRoom(rooms[0]);
    }
  }, [rooms]);

  return (
      <>
        <div className="user-chat-rooms">
          <div className="title remove-margin">
            <h1>Chat room</h1>
            <Button onClick={() => setShowModal(true)} variant="light">+ New chat room</Button>
          </div>
          <div className="user-rooms">
            {rooms.sort((room1, room2) => {
              if (room1._id === activeRoom._id) return -1;
              if (room2._id === activeRoom._id) return 1;
              return 0;
            })
                .map((room) => {
                  return (
                      <Card
                          onClick={() => selectRoom(room)}
                          className={cn("room", {
                            "active-room": room._id === activeRoom._id,
                          })}
                          key={room.id}
                          body>
                        <div>
                          <IoPersonCircleSharp size={25}/>
                        </div>
                        <p> {joinMembersByUsername(room.members)}</p>
                      </Card>
                  );
                })}
          </div>
        </div>
        <CreateRoomModal
            setCreatedRoom={setCreatedRoom}
            showModal={showModal}
            onCancelModal={() => setShowModal(false)}
            onApproveModal={(users) => createRoomForUsers(users)}
            users={getAllUserNames()}
        />
      </>
  );
};

export default UsersChatRooms;
