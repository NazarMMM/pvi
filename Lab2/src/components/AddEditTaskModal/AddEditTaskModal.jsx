import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

import "./AddEditTaskModal.scss";
export const AddEditTaskModal = ({
  showModal,
  onCancelModal,
  onApproveModal,
  mode,
  defaultValues = {},
}) => {
  const [isTaskValidated, setIsTaskValidated] = useState(false);

  const addOrEditSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    setIsTaskValidated(true);
    if (form.checkValidity()) {
      onApproveModal({
        id: defaultValues?.id,
        board: form.board.value,
        name: form.name.value,
        date: form.date.value,
        description: form.description.value,
      });
      setIsTaskValidated(false);
    }
  };

  const hideModal = () => {
    setIsTaskValidated(false);
    onCancelModal();
  };

  return (
    <>
      <Modal size="md" show={showModal} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>
            {mode === "editing" ? "Edit" : "Add"} task
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form noValidate={true} validated={isTaskValidated} onSubmit={addOrEditSubmit}>
            <Form.Group className="mb-3 form-group" controlId="board">
              <Form.Label>Board</Form.Label>
              <Form.Select defaultValue={defaultValues.board || ""} required>
                <option value="ToDo">ToDo</option>
                <option value="In Process">In Process</option>
                <option value="Done">Done</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                defaultValue={defaultValues.name || ""}
                required
                type="text"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="date">
              <Form.Label>Date</Form.Label>
              <Form.Control
                defaultValue={defaultValues.date || ""}
                required
                type="date"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                  defaultValue={defaultValues.description || ""}
                  type="text"
              />
            </Form.Group>
            <Modal.Footer>
              <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}}
                      className="save-edit-btn" type="submit">
                {mode === "editing" ? "Save" : "Add"}
              </Button>
              <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} onClick={hideModal}>
                Cancel
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
