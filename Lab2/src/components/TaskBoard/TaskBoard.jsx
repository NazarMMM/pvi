import React, {useState} from "react";
import moment from "moment";
import {Button} from "react-bootstrap";
import "./TaskBoard.scss";
import {AddEditTaskModal} from "../AddEditTaskModal/AddEditTaskModal";
import {Task} from "../Task/Task";

export const TaskBoard = ({
 tasks,
 addTask,
 editTask,
 title
}) => {
    const [addModal, setAddModal] = useState(false);

    const handleCreatingTask = (task) => {
        setAddModal(false);
        addTask({
            ...task,
            date: moment(task.date).format("DD.MM.YYYY")
        });
    };

    return (
        <>
            <div className="task-container">
                <div className="board-title">
                    {title}
                </div>
                <div className="board-tasks">
                    {tasks?.map((task) => {
                        return (
                            <Task editTask={editTask} task={task}>

                            </Task>
                        );
                    })}
                </div>
                <Button onClick={() => setAddModal(true)} className="task-add-btn" variant="light">
                    + Add task
                </Button>
            </div>
            <AddEditTaskModal
                showModal={addModal}
                onCancelModal={() => setAddModal(false)}
                onApproveModal={handleCreatingTask}
            />
        </>
    );
};
