import React, {useEffect, useState} from "react";
import {createTaskReq, getTasks, getTasksReq, updateTaskReq,} from "../../service/studentService";
import "./TaskPage.scss";
import {TaskBoard} from "../TaskBoard/TaskBoard";

const TaskPage = () => {
    const [tasks, setTasks] = useState([]);

    const createTask = async (task) => {
        try {
            await createTaskReq(task);
            await fetchTasks();
        } catch (error) {
            console.log(error);
            throw error;
        }
    };

    const updateTask = async (task) => {
        try {
            await updateTaskReq(task);
            await fetchTasks();
        } catch (error) {
            console.log(error);
            throw error;
        }
    };

    const fetchTasks = async () => {
        try {
            const tasks = await getTasksReq();
            setTasks(tasks);
        } catch (error) {
            console.log(error);
            throw error;
        }
    };

    const toDoTasks = () => {
        return tasks?.filter((task) => task.board === 'ToDo');
    };
    const inProcessTasks = () => {
        return tasks?.filter((task) => task.board === 'In Process');
    };
    const doneTasks = () => {
        return tasks?.filter((task) => task.board === 'Done');
    };

    useEffect(() => {
        try {
            fetchTasks();
        } catch (error) {
            console.log(error);
            throw error;
        }
    }, []);


    return (
        <div className="tasks-board">
            <h1 className="tasks-title">Tasks</h1>
            <div className='todo-tasks'>
                <TaskBoard
                    tasks={toDoTasks()}
                    addTask={createTask}
                    editTask={updateTask}
                    title='ToDo'
                />
            </div>
            <div className='inProcess-tasks'>
                <TaskBoard
                    tasks={inProcessTasks()}
                    addTask={createTask}
                    editTask={updateTask}
                    title='In Process'
                />
            </div>
            <div className='done-tasks'>
                <TaskBoard
                    tasks={doneTasks()}
                    addTask={createTask}
                    editTask={updateTask}
                    title='Done'
                />
            </div>
        </div>
    );
};

export default TaskPage;
