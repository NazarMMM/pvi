import React, {useRef} from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import {Form} from "react-bootstrap";
import "./CreateRoomModal.scss";
import {createRoom} from "../../service/studentService";
import {useNavigate} from "react-router-dom";


export const CreateRoomModal = ({
 showModal,
 onCancelModal,
 onApproveModal,
 setCreatedRoom,
 users
}) => {
    const formRef = useRef(null);
    const navigate = useNavigate();


    const print = async (event) => {
        event.preventDefault();

        const form = formRef.current;
        const selectedUsers = [];

        for (let i = 0; i < form.elements.length; i++) {
            const element = form.elements[i];
            if (element.type === "checkbox" && element.checked) {
                const userId = element.id;
                selectedUsers.push(userId);
            }
        }

        const res = await createRoom(selectedUsers);
        console.log(res);
        setCreatedRoom(res._id);
        navigate("/students");
    }
    return (
        <>
            <Modal show={showModal} onHide={onCancelModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Create new chat</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form noValidate={true} onSubmit={print} ref={formRef}>
                        <Form.Label>Select users</Form.Label>
                        {users?.map(user => {
                            return <Form.Group className="mb-3 form-group form-group-create" controlId={user._id}>
                            <Form.Check type="checkbox" className="remove-width" label={user.username}/>
                        </Form.Group>})}
                        <Modal.Footer>
                            <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} type="submit">
                                Ok
                            </Button>
                            <Button style={{backgroundColor: "transparent", color: "black", borderColor: "black"}} onClick={onCancelModal}>
                                Cancel
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );
};
