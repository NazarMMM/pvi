import './App.scss';
import Header from './components/Header/Header';
import MainNavbar from './components/Navbar/Navbar';
import {Navigate, Route, Routes} from 'react-router-dom';
import TableStudents from './components/Students/TableStudents';
import Login from './components/Login/Login';
import {useEffect, useState} from 'react';
import {connectToAllRooms} from "./service/studentService";
import ChatPage from "./components/ChatPage/ChatPage";
import io from "socket.io-client";
import TaskPage from "./components/Tasks/TaskPage";


function App() {
    const [currentUser, setCurrentUser] = useState(null);
    const [createdRoom, setCreatedRoom] = useState(null);
    const socket = io("http://localhost:5001");

    const connectUserToRooms = async () => {
        await connectToAllRooms(socket, currentUser);
    }

    useEffect(() => {
        if (currentUser) {
            connectUserToRooms();
        }
    }, [currentUser])
    return (
        <>
            {
                currentUser ?
                    <>
                        <Header socket={socket} id={currentUser} createdRoom={createdRoom}/>
                        <div className='main-container'>
                            <MainNavbar/>
                            <Routes>
                                <Route path='/students' element={<TableStudents/>}/>
                                <Route path='chat' element={<ChatPage socket={socket} createdRoom={createdRoom} setCreatedRoom={setCreatedRoom} currentUser={currentUser}/>}/>
                                <Route path='/tasks' element={<TaskPage/>}/>
                                <Route path='' element={<Navigate to='/students' replace/>}/>
                            </Routes>
                        </div>

                    </>
                    :
                    <Login setCurrentUser={setCurrentUser}/>
            }
        </>
    );
}

export default App;
