import axios from 'axios';
const URL = 'http://localhost/server/controller.php';
const URL2 = 'http://localhost/server/controller2.php';
const NODE_URL = 'http://localhost:5000/';

export const getTasksReq = async () => {
    try {
        const response = await axios.get(URL2, {
            params: {
                tasks: true
            }
        });
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const createTaskReq = async (task) => {
    try {
        console.log(task);
        const response = await axios.post(URL2, JSON.stringify(task));
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const updateTaskReq = async (task) => {
    try {
        console.log(task);
        const response = await axios.put(URL2, JSON.stringify(task));
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const getStudentsReq = async () => {
    try {
        const response = await axios.get(URL, {
            params: {
                students: true
            }
        });
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const getGroupsReq = async () => {
    try {
        const response = await axios.get(URL, {
            params: {
                groups: true
            }
        });
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const createStudentReq = async (student) => {
    try {
        console.log(student);
        const response = await axios.post(URL, JSON.stringify(student));
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const updateStudentReq = async (student) => {
    try {
        const response = await axios.put(URL, JSON.stringify(student));
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const deleteStudentByIdReq = async (id) => {
    try {
        const response = await axios.delete(URL + `?id=${id}`);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const login = async (loginParams) => {
    try {
        const response = await axios.post(NODE_URL + 'login', loginParams);
        return response.data;
    } catch (error) {
        throw error
    }
}

export const getAllUsers = async () => {
    try {
        const response = await axios.get(NODE_URL)
        return response.data
    } catch (error) {
        console.error(error);
        throw error
    }
}

export const getUserById = async (id) => {
    try {
        const response = await axios.get(NODE_URL + `user?id=${id}`);
        return response.data[0];
    } catch (error) {
        throw error;
    }
}

export const getRoomsByUserId = async (id) => {
    try {
        const response = await axios.get(NODE_URL + `rooms?id=${id}`);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const createRoom = async (users) => {
    try {
        const response = await axios.post(NODE_URL + `rooms`, users);
        // window.location.reload();
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const getLastThreeOrLessMessages = async (id, count) => {
    try {
        const response = await axios.get(NODE_URL + `rooms/last-messages?id=${id}&count=${count || 3}`);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const connectToAllRooms = async (socket, userId) => {
    const rooms = await getRoomsByUserId(userId);
    for (const room of rooms) {
        socket.emit("join", room._id, userId);
    }
}
