<?php

$servername = "localhost";
$username = "user1";
$password = "12345";
$dbname = "cms_students";

try {
	$pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	http_response_code(500);
	die(json_encode(array('error' => "Could not connect to db")));
}

?>