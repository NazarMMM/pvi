<?php

include 'task.php';

class TaskDAO {
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    private function getTaskById($taskId) {
          $studentIds = $this->pdo->prepare("SELECT id FROM students WHERE id = ?");
          $studentIds->execute([$studentId]);
          return $studentIds;
     }

    public function getTasks() {
            $stmt = $this->pdo->prepare('SELECT tasks.* FROM tasks');
           	$stmt->execute();
           	return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getBoards() {
            $stmt = $this->pdo->prepare('SELECT tasks.board FROM tasks');
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function createTask($task) {
           		$stmt = $this->pdo->prepare('INSERT INTO tasks (board, name, date, description) VALUES (?, ?, ?, ?)');
           		$stmt->execute([$task['board'],
           			            $task['name'],
           			            $task['date'],
           			            $task['description']]);
           	return $task;
    }

    public function updateTask($task) {
           		$stmt = $this->pdo->prepare('UPDATE tasks SET board = ?, name = ?, date = ?, description = ? WHERE id = ?');
           		$stmt->execute([
           			$task['board'],
           			$task['name'],
           			$task['date'],
           			$task['description'],
           			$task['id']
           		]);
           	return $task;
    }

    public function newTaskObj($data) {
            return new Task($data['board'], $data['name'],
                               $data['date'], $data['description']);
    }
}
?>