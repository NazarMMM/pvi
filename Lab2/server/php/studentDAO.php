<?php

include 'student.php';

class StudentDAO {
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
      }

    public function verifyStudent($student, $verifyId) {
    	$nameRegex = "/^[a-zA-Zа-яА-Я]{1,}$/";
    	$genders = array('male', 'female');
    	$response = [];

    	if (!preg_match($nameRegex, $student->getFirstName()) || !preg_match($nameRegex, $student->getLastName())) {
    		$response["error"] = "First name or Last name is incorrect!";
    	}

    	if (!($this->getStudentGroupById($student->getGroupId())->fetchColumn())) {
    		$response["error"] .= "\nGroup is incorrect!";
    	}

    	if (!in_array($student->getGender(), $genders)) {
    		$response["error"] .= "\nGender is incorrect!";
    	}

    	$minYear = 1950;
    	$maxYear = 2010;
    	$currentYear = date('Y', strtotime($student->getBirthday()));

    	if ($currentYear > $maxYear || $currentYear < $minYear) {
    		$response["error"] .= "\nBirthday date must be between " . $minYear . " and " . $maxYear;
    	}

    	if ($verifyId) {
    		if (!($this->getStudentById($student->getId())->fetchColumn())) {
    			$response["error"] .= "\nThere is not student with this id";
    		}
    	}

    	$response["firstName"] = $student->getFirstName();
    	$response["lastName"] =  $student->getLastName();
    	$response["group"] = ($this->getStudentGroupById($student->getGroupId())->fetchColumn());
    	$response["birthday"] = date('Y-m-d', strtotime($student->getBirthday()));
    	$response["gender"] = $student->getGender();
    	$response["id"] = $student->getId();

    	return $response;
    }

    private function getStudentGroupById($groupId) {
        	$groupsList = $this->pdo->prepare("SELECT id FROM student_groups WHERE id = ?");
        	$groupsList->execute([$groupId]);
        	return $groupsList;
      }

    private function getStudentById($studentId) {
          $studentIds = $this->pdo->prepare("SELECT id FROM students WHERE id = ?");
          $studentIds->execute([$studentId]);
          return $studentIds;
     }

    public function getStudents() {
            $stmt = $this->pdo->prepare('SELECT students.*, student_groups.name AS `group`FROM students
            INNER JOIN student_groups ON students.group_id = student_groups.id');
           	$stmt->execute();
           	return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getGroups() {
            $stmt = $this->pdo->prepare('SELECT * FROM student_groups');
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function createStudent($student) {
            $verifiedStudent = $this->verifyStudent($student, false);
           	if (!$verifiedStudent['error']) {
           		$stmt = $this->pdo->prepare('INSERT INTO students (firstname, lastname, birthday, group_id, gender) VALUES (?, ?, ?, ?, ?)');
           		$stmt->execute([$verifiedStudent['firstName'],
           			            $verifiedStudent['lastName'],
           			            $verifiedStudent['birthday'],
           			            $verifiedStudent['group'],
           			            $verifiedStudent['gender']]);
           	}
           	return $verifiedStudent;
    }

    public function updateStudent($student) {
           	$verifiedStudent = $this->verifyStudent($student, true);

           	if (!$verifiedStudent['error']) {
           		$stmt = $this->pdo->prepare('UPDATE students SET firstname = ?, lastname = ?, birthday = ?, group_id = ?, gender = ? WHERE id = ?');
           		$stmt->execute([
           			$verifiedStudent['firstName'],
           			$verifiedStudent['lastName'],
           			$verifiedStudent['birthday'],
           			$verifiedStudent['group'],
           			$verifiedStudent['gender'],
           			$verifiedStudent['id']
           		]);
           	}
           	return $verifiedStudent;
    }

    public function deleteStudentById($id) {
           	$stmt = $this->pdo->prepare('DELETE FROM students WHERE id = ?');
           	$success = $stmt->execute([$id]);
           	if ($success) {
           		header('Content-Type: application/json');
           	}
           	return $success;
    }

    public function newStudentObj($data) {
            return new Student($data['id'], $data['firstName'], $data['lastName'],
                               $data['gender'], $data['group'], $data['birthday']);
    }
}
?>