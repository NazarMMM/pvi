<?php
class Task {
      private $id;
      private $name;
      private $description;
      private $board;
      private $date;

      public function __construct($board, $name, $date, $description) {
          $this->board = $board;
          $this->name = $name;
          $this->date = $date;
          $this->description = $description;
      }

      public function getId() {
          return $this->id;
      }
      public function getBoard() {
                return $this->board;
       }
      public function getName() {
               return $this->name;
       }
      public function getDate() {
              return $this->date;
      }
      public function getDescription() {
              return $this->description;
       }

      public function setBoard($board) {
        $this->board = $board;
      }
      public function setName($name) {
              $this->name = $name;
      }
      public function setDate($date) {
         $this->date = $date;
      }
      public function setDescription($description) {
             $this->description = $description;
      }

}
?>