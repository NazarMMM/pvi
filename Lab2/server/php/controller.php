<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: Content-Type');
error_reporting(E_ERROR | E_PARSE);

require_once 'config.php';
require_once 'studentDAO.php';

$studentDAO = new StudentDAO($pdo);

if (checkRequestMethod('GET') && isset($_GET['students'])) {
	$students = $studentDAO->getStudents();
	header('Content-Type: application/json');
	echo json_encode($students);
}

if (checkRequestMethod('GET') && isset($_GET['groups'])) {
	$groups = $studentDAO->getGroups();
	header('Content-Type: application/json');
	echo json_encode($groups);
}

if (checkRequestMethod('POST') && isset($_GET['login'])) {
	$loginParams = json_decode(file_get_contents('php://input'), true);
    login($loginParams);
}

function login($loginParams) {
    global $pdo;
		$stmt = $pdo->prepare('SELECT id FROM login WHERE username = :username AND password = :password');
		$stmt->execute([
			'username' => $loginParams['username'],
			'password' => $loginParams['password']
		]);
		$result = $stmt->fetch();
		if ($result) {
			echo json_encode($result);
		} else {
			http_response_code(400);
			echo json_encode(["error" => "could not find user"]);
		}
		exit();

	http_response_code(404);
}

if (checkRequestMethod('POST')) {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = $studentDAO->createStudent($studentDAO->newStudentObj($data));
	if ($result['error']) {
	    http_response_code(400);
	} else {
	    http_response_code(200);
	}
	echo json_encode($result);
}

if (checkRequestMethod('PUT')) {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = $studentDAO->updateStudent($studentDAO->newStudentObj($data));
	if ($result['error']) {
	    http_response_code(400);
	} else {
      	http_response_code(200);
    }
	echo json_encode($result);
}

if (checkRequestMethod('DELETE')) {
	$id = $_GET['id'];
    $success = $studentDAO->deleteStudentById($id);
    if (!$success) {
	    http_response_code(400);
	    echo json_encode(['error' => "Error with deletion"]);
    }
}

function checkRequestMethod($method) {
    return $_SERVER['REQUEST_METHOD'] === $method;
}

?>