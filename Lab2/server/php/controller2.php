<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: Content-Type');
error_reporting(E_ERROR | E_PARSE);

require_once 'config.php';
require_once 'taskDAO.php';

$taskDAO = new TaskDAO($pdo);

if (checkRequestMethod('GET') && isset($_GET['tasks'])) {
	$tasks = $taskDAO->getTasks();
	header('Content-Type: application/json');
	echo json_encode($tasks);
}

if (checkRequestMethod('POST')) {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = $taskDAO->createTask($data);
	if ($result['error']) {
	    http_response_code(400);
	} else {
	    http_response_code(200);
	}
	echo json_encode($result);
}

if (checkRequestMethod('PUT')) {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = $taskDAO->updateTask($data);
	if ($result['error']) {
	    http_response_code(400);
	} else {
      	http_response_code(200);
    }
	echo json_encode($result);
}

function checkRequestMethod($method) {
    return $_SERVER['REQUEST_METHOD'] === $method;
}

?>