import mongoose from "mongoose";

const Message = new mongoose.Schema({
	room: { type: "string", required: true },
	from: { type: "string", required: true },
	message: { type: "string", required: true },
	createdAt: { type: Date, required: false, default: Date.now }
})

export default mongoose.model('Message', Message)