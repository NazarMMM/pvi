import mongoose from "mongoose";

const User = new mongoose.Schema({
	username: { type: "string", required: true },
	password: { type: "string", required: true },
})

export default mongoose.model('User', User)