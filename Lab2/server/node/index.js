import express from 'express';
import User from './user-schema.js'
import Message from './message-schema.js'
import ChatRoom from './chat-room-schema.js'
import { Server } from "socket.io";
import mongoose from 'mongoose';

const PORT = 5000;
const DATABASE = 'mongodb+srv://nazar4uk:nazar4uk@pvi.xeqpute.mongodb.net/pvi?retryWrites=true&w=majority';
const app = express();

app.use(express.json());

app.post('/', async (request, response) => {
	const { username, password } = request.body;
	const user = await User.create({ username, password });
	response.status(200).json(user._id);
})

app.get('/user', async (request, response) => {
	const id = request.query.id;
	const user = await User.find({ _id: id });
	response.status(200).json(user);
})

app.get('/', async (request, response) => {
	const users = await User.find({});
	response.status(200).json(users);
})

app.post('/login', async (request, response) => {
	const { username, password } = request.body;
	const user = await User.findOne({ username, password })
	if (user) {
		response.status(200).json(user.id)
	} else {
		response.status(400).json("Could not find user with username " + username)
	}
})

app.get('/rooms', async (request, response) => {
	const id = request.query.id;
	const rooms = await ChatRoom.find({ members: id });
	response.status(200).json(rooms);
})

app.post('/rooms', async (request, response) => {
	const users = request.body;
	console.log(users);
	const rooms = await ChatRoom.create({ members: users });
	response.status(200).json(rooms);
})

app.get('/rooms/last-messages', async (request, response) => {
	const userId = request.query.id;
	const messagesCount = request.query.count ?? 3;
	const rooms = await ChatRoom.find({ members: userId });
	const messages = await Message.find({ room: { $in: rooms.map(room => room._id) }, from: {$ne:userId} })
		.sort({ createdAt: -1 })
		.limit(messagesCount);
	response.status(200).json(messages);
})

const io = new Server(5001, {})

io.on('connection', socket => {
	console.log('User got connected');
	socket.on('join', async (roomId, userId) => {
		let room = roomId;
		const roomExists = await ChatRoom.exists({ _id: roomId });
		if (!roomExists) {
			const newRoom = await ChatRoom.create({ members: [userId] });
			room = newRoom.id;
		}

		socket.join(room);
		socket.to(room).emit('userJoined', userId);

		const messages = (await Message.find({ room })
			.sort('-createdAt')
			.exec())
			?.reverse();
		socket.emit('messageHistory', messages);
	});

	socket.on('message', async (room, from, message) => {
		const newMessage = await Message.create({ room, from, message });
		if (newMessage) {
			io.to(room).emit('message', newMessage);
		} else {
			console.log('could not send message');
		}
	});

	socket.on('disconnect', () => {
		console.log('User got disconnected');
	});
});



const appBootstrap = async () => {
	try {
		await mongoose.connect(DATABASE);
		app.listen(PORT, () => {
			console.log('node server works with db');
		});
	} catch (error) {
		throw error;
	}
}

appBootstrap();