import React from "react";
import { Button, Form } from "react-bootstrap";
import { login } from "../../service/studentService";
import "./Login.scss";
const Login = ({ setCurrentUser }) => {
    const loginUser = async (event) => {
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;
        if (form.checkValidity()) {
            try {
                const response = await login({
                    username: form.username.value,
                    password: form.password.value,
                });
                setCurrentUser(response);
            } catch (error) {
                setCurrentUser(null);
                alert("Incorrect login");
            }
        }
    };

    return (
        <div className="login">
            <h1>Login</h1>
            <Form autocomplete="off" className="center-elements" onSubmit={loginUser}>
                <Form.Group className="mb-3 form-group center-inputs" controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        autocomplete="off"
                        required
                        type="text"
                        placeholder="username"
                    />
                </Form.Group>
                <Form.Group className="mb-3 form-group center-inputs" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        autocomplete="off"
                        required
                        type="password"
                        placeholder="password"
                    />
                </Form.Group>
                <Button className="btn-color" type="submit">Log in</Button>
            </Form>
        </div>
    );
};

export default Login;